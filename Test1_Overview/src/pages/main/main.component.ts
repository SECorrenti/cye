
module sec {


    interface IMainController {
        mainModel: IMainModel;        
    }


    class MainController implements IMainController {

        public readonly mainModel: IMainModel;

        static $inject = ["mainService"];

        constructor(mainService: sec.IMainService){
            this.mainModel = mainService.getModel();
        }

    }


    class MainComponent {

        static instance: ng.IComponentOptions = {
            
            template: ['$templateCache', ($TC) => $TC.get('src/pages/main/main.component.html')],
            controller: MainController, 
            controllerAs: 'mainCtrl'

        }

    }


    angular.module('sec').component('mainComponent', MainComponent.instance);

}


