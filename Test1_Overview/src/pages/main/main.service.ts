
module sec {


    export interface IMainService {
        getModel(): MainModel
    }

    export class MainService implements IMainService {

        mainModel = new MainModel();

        getModel(): IMainModel {
            return this.mainModel;
        }
    }


    angular.module('sec').service('mainService', MainService);

}