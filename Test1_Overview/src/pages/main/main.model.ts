

module sec {


    export interface IMainModel {
        items: Array<IItemModel>;
        ranks: Array<IRankModel>;
        details: Array<IDetailsModel>;
    }


    export class MainModel implements IMainModel {
        
        public readonly items: Array<IItemModel>;
        public readonly ranks: Array<IRankModel>;
        public readonly details: Array<IDetailsModel>;
        
        constructor() {
            this.items = [
                new ItemModel('assets/cube.png', 'Type', 'Standart'),
                new ItemModel('assets/check.png', 'Framework', 'NIST'),
                new ItemModel('assets/calendar.png', 'Year founded', '1982'),
                new ItemModel('assets/users.png', 'Employees', '300+'),
                new ItemModel('assets/location.png', 'HQ', 'London, Oxford WW2 A`'),
            ];
            this.ranks = [
                new RankModel('Industry Rank', 'Technology', 42, 250, 65, true),
                new RankModel('Region Rank', 'United States', 72, 124, 70),
                new RankModel('Global Rank', 'Wordwide', 98, 1420, 90, true),
            ];
            this.details = [
                new DetailsModel('#00b07b', 82, 'Identify', 50, 'Overall tests'),
                new DetailsModel('#f2c549', 76, 'Protect', 32, 'Overall tests'),
                new DetailsModel('#00b07b', 88, 'Detect', 198, 'Overall tests'),
                new DetailsModel('#ff8430', 52, 'Respond', 70, 'Overall tests'),
                new DetailsModel('#f2c549', 74, 'Recover', 120, 'Overall tests'),
            ];
        }
    }

    
}

