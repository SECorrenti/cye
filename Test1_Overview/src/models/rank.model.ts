

module sec {


    export interface IRankModel {
        name: string;
        location: string;
        value: number;
        counter: number;
        percent: number;
        up: boolean;
    }


    export class RankModel implements IRankModel {
        
        constructor(
            public name: string,
            public location: string,
            public value: number,
            public counter: number,
            public percent: number,
            public up: boolean = false
        ) {
        }
    }

    
}

