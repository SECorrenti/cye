

module sec {


    export interface IDetailsModel {
        color: string;
        identifier: number;
        name: string;
        value: number;
        description: string;
    }

    export class DetailsModel implements IDetailsModel {

        constructor(
            public color: string,
            public identifier: number,
            public name: string,
            public value: number,
            public description: string,
        ) {
        }
    }


}

