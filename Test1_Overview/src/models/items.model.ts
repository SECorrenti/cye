

module sec {


    export interface IItemModel {
        path: string;
        value: string,
        type: string;
    }


    export class ItemModel implements IItemModel {
        
        constructor(
            public path: string,
            public value: string,
            public type: string
        ) {
        }
    }

    
}

