
module sec {

    function SecLogoDirective(): ng.IDirective {

        var directive = <ng.IDirective> {
            restrict : 'E',
            template: `
                <div class="logo">
                    <img src="assets/logo.png" alt="logo">
                    <h1>Overview</h1>
                </div>
            `,
        };

        return directive;
    }

    angular.module('sec').directive('secLogo', SecLogoDirective);

}