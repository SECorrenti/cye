
module sec {

    class SecHexagonalComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                color: "<",
                identifier: "<",
                hint: '@',
            },
            controllerAs: 'hexCtrl',
            template: ['$templateCache', ($TC) => $TC.get('src/widgets/hexagonals-details/hexagonal/hexagonal.component.html')],
        };

    }

    angular.module('sec').component('secHexagonal', SecHexagonalComponent.instance)
}
