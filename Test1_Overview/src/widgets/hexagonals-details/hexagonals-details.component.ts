
module sec {

    class SecHexagonalsDetailsComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                model: '<',
            },
            controllerAs: 'hexDetCtrl',
            template: ['$templateCache', ($TC) => $TC.get('src/widgets/hexagonals-details/hexagonals-details.component.html')],
        };

    }

    angular.module('sec').component('secHexagonalsDetails', SecHexagonalsDetailsComponent.instance)
}
