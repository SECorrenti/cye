
module sec {

    class SecHexagonalDataComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                model: '<',
                hint: '@?'
            },
            controllerAs: 'hexDataCtrl',
            template: ['$templateCache', ($TC) => $TC.get('src/widgets/hexagonals-details/hexagonal-data/hexagonal-data.component.html')],
        };

    }

    angular.module('sec').component('secHexagonalData', SecHexagonalDataComponent.instance)
}
