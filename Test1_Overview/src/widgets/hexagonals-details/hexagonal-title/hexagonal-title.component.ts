
module sec {

    class SecHexagonalTitleComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                text: "@",
            },
            controllerAs: 'hexTitleCtrl',
            template: `
                {{::hexTitleCtrl.text}}
                <img src="assets/info.svg" alt="info">
            `,
        };

    }

    angular.module('sec').component('secHexagonalTitle', SecHexagonalTitleComponent.instance)
}
