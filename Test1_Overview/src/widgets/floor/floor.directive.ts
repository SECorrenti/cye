
module sec {

    function SecFloorDirective(): ng.IDirective {

        var directive = <ng.IDirective> {
            restrict : 'E',
            template: `
                <div class="floor">
                    <div></div>
                </div>
            `,
        };

        return directive;
    }

    angular.module('sec').directive('secFloor', SecFloorDirective);

}

