
module sec {

    class SecItemsComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                model: '<',
            },
            controllerAs: 'itemsCtrl',
            template: `
                <ul class="items">
                    <li data-ng-repeat="item in itemsCtrl.model">
                        <img data-ng-src="{{::item.path}}" alt="cube">
                        <label>{{::item.value}}</label>
                        <span>{{::item.type}}</span>
                    </li>
                </ul>
            `,
        };

    }

    angular.module('sec').component('secItems', SecItemsComponent.instance);

}