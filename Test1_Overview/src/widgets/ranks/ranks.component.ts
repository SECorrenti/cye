
module sec {

    export interface ISecRanksController extends ng.IController {
        setCanvas(element: HTMLCanvasElement, rank: IRankModel): void;
    }

    class SecRanksController implements ISecRanksController {

        setCanvas(canvas: HTMLCanvasElement, rank: IRankModel): void {
            const data = {
                datasets: [{
                    data: [rank.percent, 100 - rank.percent],
                    backgroundColor: [rank.up ? '#04a770' : '#dc2e47', '#d8dae2',],
                }],
            };
            const options: Chart.ChartOptions = {
                cutoutPercentage: 80,
                tooltips: { enabled: false }
            }
            new Chart(canvas, { type: 'doughnut', data, options });
        }

    }


    class SecRanksComponent {

        static instance: ng.IComponentOptions = {
            bindings: {
                model: '<',
            },
            controllerAs: 'ranksCtrl',
            template: `
            <article class="ranks">
                <section data-ng-repeat="rank in ranksCtrl.model">
                    <div>
                        <h4>{{::rank.name}}</h4>
                        <h5>{{::rank.location}}</h5>
                    </div>
                    <canvas sec-canvas="rank"></canvas>
                    <div class="circle">
                        <label data-ng-class="{ up : rank.up }">
                            {{::rank.value}}
                        </label>
                        <span>/{{::rank.counter}}</span>
                    </div> 
                </section>
            </article>
            `,
            controller: SecRanksController
        };

    }

    angular.module('sec').component('secRanks', SecRanksComponent.instance);

}
