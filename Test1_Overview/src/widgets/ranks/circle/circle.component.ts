

module sec {

    function SecCanvasDirective(): ng.IDirective {

        var directive = <ng.IDirective>{
            scope: {
                secCanvas: '<'
            },
            restrict: 'A',
            require: {
                rankCtrl: '^secRanks',
            },
            link: {
                pre: link
            }
        };

        function link(
            scope: ICanvasScope,
            element: JQLite,
            attrs: ng.IAttributes,
            controller: ISecRanksController): void {
                console.log(this);
                controller.rankCtrl.setCanvas(element[0], scope.secCanvas);
        }

        return directive;
    }

    angular.module('sec').directive('secCanvas', SecCanvasDirective);

}


interface ICanvasScope extends ng.IScope {
    secCanvas: string,
}


interface IPostControllers extends ng.IController {
    rankCtrl: ng.IController,
}
