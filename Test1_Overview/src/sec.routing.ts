
((): void => {
    'use strict';

    angular
        .module('sec')
        .config(config);

    config.$inject = [
        '$stateProvider',
        '$urlRouterProvider'
    ];
    function config(
        $stateProvider: any,
        $urlRouterProvider: any): void {
            
        $stateProvider.state({
            name: 'main',
            url: '/',
            component: 'mainComponent'
        });

        $urlRouterProvider.otherwise('/');
    }
})();