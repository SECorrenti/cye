# Installation
* `npm i`


# Must Grunt, Ruby and Sass installed
* `npm i -g grunt-cli`
* `gem install sass`
* `gem install compass` (4 windows)


# TypeSearch
* `https://microsoft.github.io/TypeSearch`



# Run
* `grunt watch`
* `live-server` Or `node node_modules\http-server\bin\http-server`
* Serve browser on `http://localhost:8080/index.html#!/main`

