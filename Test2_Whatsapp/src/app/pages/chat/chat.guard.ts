import { Injectable } from '@angular/core';
import { AppService, } from 'src/app/app.service';
import { CanDeactivate } from '@angular/router';

@Injectable()
export class ChatGuard implements CanDeactivate<void> {

  constructor(private appService: AppService) {
  }

  canDeactivate(): boolean {
    this.appService.signout();
    return true;
  }
}
