import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { IUsersModel } from 'src/app/models/user.model';
import { AppService } from 'src/app/app.service';
import { UserMessage, IUserMessage } from 'src/app/models/user-message.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnDestroy {

  private readonly subs = new Subscription();

  admin: IUsersModel;
  messages: Array<IUserMessage> = [];
  userId: number;

  constructor(private route: ActivatedRoute, private appService: AppService) {

    this.admin = this.appService.getAdmin();

    this.subs.add(this.route.params.subscribe(params => {
        this.userId = +params.userId;
        this.updateAll();
    }));

    this.subs.add(this.appService.usersListener
      .subscribe(() => this.updateAll()));

  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private updateAll() {
    this.loadMessages();
    let haveChanges = false;
    this.messages.map(message => {
      if (message.sender === this.userId) {
        haveChanges = true;
        message.isReaded = true;
      }
    });
    if (haveChanges) {
      this.appService.updateMessages(this.bindKey(), this.messages);
    }
  }

  addMessage(text: string): void {
    this.appService.addMessage(this.bindKey(), new UserMessage(Date.now(), text, this.admin.id));
  }

  private bindKey() {
    const key = this.admin ? [this.admin.id, this.userId].sort((a, b) => a - b) : '';
    return 'conv_' + key[0] + '-' + key[1];
  }

  private loadMessages() {
    this.messages = this.appService.getMessages(this.bindKey());
  }

}
