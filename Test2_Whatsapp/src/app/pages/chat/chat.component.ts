import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import { IUsersModel } from 'src/app/models/user.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnDestroy {

  users: Array<IUsersModel> = [];
  admin: IUsersModel;
  activeUser: number;

  @HostListener('window:unload')
  unloadHandler() {
    this.appService.signout();
  }

  constructor(
    private appService: AppService,
    private router: Router,
    ) {
    this.users = appService.getUsers();
    this.admin = appService.getAdmin();
    this.appService.usersListener
      .subscribe(users => {
        this.users = users;
      });

    if (!this.admin) {
      router.navigate(['./login']);
    }

  }

  ngOnDestroy(): void {
    this.appService.signout();
  }

  redirect(user: IUsersModel): void {
    this.activeUser = user.id;
    this.router.navigate(['chat', 'conversation', user.id]);
  }

}
