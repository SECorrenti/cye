import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ChooseAvatarDialogComponent } from 'src/app/dialogs/choose-avatar/choose-avatar.component';
import { IUsersModel, UsersModel } from 'src/app/models/user.model';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  myForm = new FormGroup({
    id: new FormControl(Date.now()),
    name: new FormControl(null, Validators.required),
    avatar: new FormControl(null, Validators.required),
  });

  constructor(
    private appService: AppService,
    private router: Router,
    public dialog: MatDialog
  ) {

  }

  onSubmit() {
    const values: IUsersModel = this.myForm.value;
    const user = new UsersModel(values.id, values.name, values.avatar);
    this.appService.signin(user);
    this.router.navigateByUrl('/chat');
  }

  openImages() {
    const dialogRef = this.dialog.open(ChooseAvatarDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.myForm.controls.avatar.setValue(result);
      this.myForm.updateValueAndValidity();
    });
  }

  quick(img: string) {
    this.myForm.controls.name.setValue('סרחיו קורנטי');
    if (img) {
      this.myForm.controls.avatar.setValue(img);
      this.onSubmit();
    } else {
      this.openImages();
    }
  }

}
