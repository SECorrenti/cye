

export interface IMessageDictionary {
  [key: string]: Array<IUserMessage>;
};

export interface IUserMessage {
  id: number;
  text: string;
  sender: number;
  isReaded: boolean;
}


export class UserMessage implements IUserMessage {
  public isReaded = false;
  constructor(
    public id: number,
    public text: string,
    public sender: number,
  ) { }
}
