

export interface IUsersModel {
  id: number;
  name: string;
  avatar: string;
  unread: number;
}


export class UsersModel implements IUsersModel {
  public unread: number;
  constructor(
    public id: number,
    public name: string,
    public avatar: string,
  ) { }
}

