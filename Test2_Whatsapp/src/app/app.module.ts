import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContactComponent } from './pages/contact/contact.component';
import { WidgetsModule } from './widgets/widgets.module';
import { LoginComponent } from './pages/login/login.component';
import { ChatComponent } from './pages/chat/chat.component';
import { RoomComponent } from './pages/chat/room/room.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocalMaterialModule } from 'src/shared/material.module';
import { DialogsModule } from './dialogs/dialogs.module';
import { ConversationComponent } from './pages/chat/conversation/conversation.component';
import { ChatInputComponent } from './widgets/chat-input/chat-input.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    LoginComponent,
    ChatComponent,
    RoomComponent,
    ConversationComponent,
    ChatInputComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    WidgetsModule,
    LocalMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
