import { Injectable, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { IUsersModel, UsersModel } from './models/user.model';
import { IUserMessage, IMessageDictionary } from './models/user-message.model';

@Injectable({
  providedIn: 'root',
})

export class AppService implements OnDestroy {

  private readonly keys = eLocalStorageKeys;
  private users: Array<IUsersModel>;
  private admin: IUsersModel;
  private messages: IMessageDictionary = {};
  usersListener = new Subject<Array<IUsersModel>>();

  constructor() {
    this.enableInterval();
  }

  ngOnDestroy(): void {
    if (this.admin) {
      const users = this.users.filter(user => user.id !== this.admin.id);
      this.saveUsers(users);
    }
  }

  getUsers() {
    return this.loadUsers();
  }

  getAdmin() {
    return this.admin;
  }

  getMessages(key: string): Array<IUserMessage> {
    return this.loadMessages()[key] || [];
  }

  setUser(user: IUsersModel) {
    const users = this.loadUsers();
    users.push(user);
    this.saveUsers(users);
  }

  signin(admin: IUsersModel) {
    this.admin = admin;
    this.setUser(this.admin);
  }

  signout() {
    this.ngOnDestroy();
  }

  addMessage(key: string, message: IUserMessage) {
    const messages = this.loadMessages();
    if (!messages[key]) {
      messages[key] = [];
    }
    messages[key].unshift(message);
    this.saveMessages(messages);
  }

  updateMessages(key: string, messages: Array<IUserMessage>) {
    const dictionary = this.loadMessages();
    dictionary[key] = messages;
    this.saveMessages(dictionary);
  }

  private saveUsers(users: Array<IUsersModel>) {
      localStorage.setItem(this.keys.users, this.Json(users));
  }

  private saveMessages(messages: IMessageDictionary) {
      localStorage.setItem(this.keys.messages, this.Json(messages));
  }

  private Json(obj: any): string {
    return JSON.stringify(obj);
  }

  private loadUsers() {
    return JSON.parse(localStorage.getItem(this.keys.users) || '[]');
  }

  private loadMessages() {
    return JSON.parse(localStorage.getItem(this.keys.messages) || '{}');
  }

  private enableInterval() {
    setInterval(() => {
      let haveChanges = false;
      const users = this.loadUsers();
      if (!haveChanges) {
        const messages = this.loadMessages();
        if (this.Json(users) != this.Json(this.users)) {
          this.users = users;
          haveChanges = true;
        }
        if (this.Json(messages) != this.Json(this.messages)) {
          this.messages = messages;
          haveChanges = true;
        }
      }
      if (haveChanges) {
        this.usersListener.next(users);
      }
    }, 300);
  }

}


enum eLocalStorageKeys {
  users = 'users',
  messages = 'messages',
  saveInProccess = 'saveinProccess',
}

