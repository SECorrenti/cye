import { Component, OnInit, Input } from '@angular/core';
import { IUsersModel } from 'src/app/models/user.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {


  @Input() admin: IUsersModel;


}
