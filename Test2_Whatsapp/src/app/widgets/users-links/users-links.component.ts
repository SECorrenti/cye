import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IUsersModel } from 'src/app/models/user.model';
import { AppService } from 'src/app/app.service';
import { IUserMessage } from 'src/app/models/user-message.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users-links',
  templateUrl: './users-links.component.html',
  styleUrls: ['./users-links.component.scss']
})
export class UsersLinksComponent implements OnInit {

  private readonly subs = new Subscription();

  @Input() user: IUsersModel;
  admin: IUsersModel;
  lastMessages: IUserMessage;
  unread: number;

  constructor(private appService: AppService) {
    this.subs.add(this.appService.usersListener
      .subscribe(() => this.bind()));
    this.admin = this.appService.getAdmin();
  }

  ngOnInit(): void {
    this.bind();
  }

  bind(): void {
    this.unread = 0;
    const messages = this.appService.getMessages(this.bindKey());
    if (messages.length) {
      this.lastMessages = messages[0];
      for (let i = 0; i < messages.length; i++) {
        const message = messages[i];
        if (!message.isReaded && message.sender !== this.admin.id) {
          this.unread++;
        } else {
          break;
        }
      }
    }
  }

  private bindKey() {
    const key = this.admin ? [this.admin.id, this.user.id].sort((a, b) => a - b) : '';
    return 'conv_' + key[0] + '-' + key[1];
  }

}
