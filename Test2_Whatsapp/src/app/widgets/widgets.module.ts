
import { NgModule } from '@angular/core';
import { WarningComponent } from './warning/warning.component';
import { UsersLinksComponent } from './users-links/users-links.component';
import { AdminComponent } from './admin/admin.component';
import { CommonModule } from '@angular/common';

const declarations = [
  AdminComponent,
  UsersLinksComponent,
  WarningComponent,
];

const imports = [
  CommonModule
];

@NgModule({
  declarations,
  imports,
  exports: [
    declarations
  ]
})
export class WidgetsModule { }
