import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-choose-avatar',
  templateUrl: './choose-avatar.component.html',
  styleUrls: ['./choose-avatar.component.scss']
})
export class ChooseAvatarDialogComponent {

  constructor(public dialogRef: MatDialogRef<ChooseAvatarDialogComponent>) { }

  close(img: string): void {
    this.dialogRef.close(img);
  }

}
