import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './pages/chat/chat.component';
import { LoginComponent } from './pages/login/login.component';
import { RoomComponent } from './pages/chat/room/room.component';
import { ChatGuard } from './pages/chat/chat.guard';
import { ConversationComponent } from './pages/chat/conversation/conversation.component';

const routes: Routes = [
  { path: '', redirectTo: 'chat', pathMatch: 'full' },
  {
    path: 'chat', component: ChatComponent, canDeactivate: [ChatGuard],
    children: [
      { path: '', redirectTo: 'room', pathMatch: 'full' },
      { path: 'room', component: RoomComponent },
      { path: 'conversation/:userId', component: ConversationComponent },
      { path: '**',  redirectTo: 'room'},
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: 'chat', },
];

@NgModule({
  providers: [ChatGuard],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
